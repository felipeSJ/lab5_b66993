package cr.ac.ucr.ecci.cql.miserviciossensores;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentManager;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.DateFormat;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity implements DownloadCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener, SensorEventListener {

    private static final int PERMISSION_REQUEST_LOCATION = 0;
    private ProgressDialog mDialog;
    private WebView mWebView;
    private ImageView mImageView;
    private TextView mDataText;

    // Mantenemos referencia a NetworkFragment que administra la AsyncTask para las operaciones de red
    private NetworkFragment mNetworkFragment;

    // Boolean para tener control si un download esta en progreso y no iniciarlo N veces
    private boolean mDownloading = false;

    // BroadcastReceiver para mensajes desde los servicios
    private BroadcastReceiver mReceiver;

    public static final String TAG_IMG = "IMG";

    protected static final String TAG = "MainActivity";

    // intervalo de chequeo de localizacion
    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 5000; //10000
    // tasa limite para las actualizaciones, no mas ferecuentes que este valor
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2;

    // API de Google para servicios de localizacion
    protected GoogleApiClient mGoogleApiClient;

    // Parametros para el request en FusedLocationProviderApi
    protected LocationRequest mLocationRequest;
    // Tipos de servicios de localizacion
    protected LocationSettingsRequest mLocationSettingsRequest;
    // Localizacion geografica
    protected Location mCurrentLocation;
    // Estatus del request
    protected boolean mRequestingLocationUpdates;
    // Tiempo de reqgistro de la localizacion
    protected String mLastUpdateTime;

    LocationCallback locationCallback;

    private FusedLocationProviderClient fusedLocationClient;

    private long ultimaActualizacion = 0;
    private float previoX = 0, previoY = 0, previoZ = 0;
    private float actualX = 0, actualY = 0, actualZ = 0;


    // Servicios SOAP
    private static final String URL = "http://www.webservicex.net/geoipservice.asmx";
    //*private static final String URL2 = "http://www.webservicex.net/uszip.asmx";
    private static final String NAMESPACE = "http://www.webservicex.net/";
    //*private static final String NAMESPACE2 = "http://www.webservicex.net/";
    private static final String METHOD_NAME = "GetGeoIP";
    // *private static final String METHOD_NAME2 = "GetInfoByZIP";
    private static final String SOAP_ACTION = "http://www.webservicex.net/GetGeoIP";
    // Servicios REST
    private static final String URL_REST = "http://services.groupkt.com/country/get/iso3code/CRI";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mDataText = findViewById(R.id.text);
        mWebView = findViewById(R.id.webView);
        mImageView = findViewById(R.id.imageView);

        mDataText.setVisibility(View.INVISIBLE);
        mWebView.setVisibility(View.INVISIBLE);
        mImageView.setVisibility(View.INVISIBLE);

        // servicios de localizacion
        mRequestingLocationUpdates = false;
        mLastUpdateTime = "";

        // Creamos los objetos GoogleApiClient, LocationRequest, LocationSettingsRequest
        buildGoogleApiClient();
        createLocationRequest();
        buildLocationSettingsRequest();

        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                   return;
                }
                Location location = locationResult.getLastLocation();
                mCurrentLocation = location;
                mLastUpdateTime = DateFormat.getDateTimeInstance().format(new Date());
                updateUI();

            }
        };

        // Servicio BroadcastReceiver
        mReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String s = intent.getStringExtra(ServicioSimple.SERVICE_MESSAGE);
                String txt = mDataText.getText().toString();
                txt += s + "\n";
                mDataText.setText(txt);
            }
        };

    }

    // Menu de opciones
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.example_one:
                // Mostramos un dialogo de barra de progreso mediante una tarea
                ejemploBarraProgreso();
                return true;
            case R.id.example_two:
                // Mostramos una vista web mediante una tarea
                ejemploVistaWeb();
                return true;
            case R.id.example_three:
                // Mostramos una imagen mediante una tarea
                ejemploMostrarImagen();
                return true;
            case R.id.example_four_init_action:
                // Iniciamos el fragmento que ejecutamos en la tarea asincronica pasando el URL que buscamos su contenido
                startFragmentTask("https://www.facebook.com");
                return true;
            case R.id.example_four_fetch_action:
                // Cuando el usuario hace clic en extraer, se obtiene los primeros N caracteres del HTML
                // Iniciamos el download de la página
                startDownload();
                return true;
            case R.id.example_four_clear_action:
                // Limpiamos el texto de resultado y cancelamos el download si este aun esta en proceso
                finishDownloading();
                mDataText.setText("Cierre de tarea.");
                return true;
            case R.id.example_five_init_action:
                // Iniciamos el fragmento que ejecutamos en la tarea asincronica pasando el URL que buscamos
                // Este devuelve un html a partir del servicion REST de Google
                startFragmentTask("https://www.google.com/search?q=universidad+costa+rica");
                return true;
            case R.id.example_five_fetch_action:
                // Cuando el usuario hace clic en extraer, se obtiene los primeros N caracteres del HTML
                // Iniciamos el download de la página
                startDownload();
                return true;
            case R.id.example_five_clear_action:
                // Limpiamos el texto de resultado y cancelamos el download si este aun esta en proceso finishDownloading();
                mDataText.setText("Cierre de tarea.");
                return true;
            case R.id.example_six_init_action:
                // Iniciamos el servicio
                inicioServicio();
                return true;
            case R.id.example_six_clear_action:
                // Detenemos el servicio
                finServicio();
                return true;
            case R.id.example_seven_connect_action:
                // Servicio de localizacion
                conectarServicioLocalizacion();
                return true;
            case R.id.example_seven_init_action:
                // Servicio de localizacion
                iniciarServicioLocalizacion();
                return true;
            case R.id.example_seven_clear_action:
                // Detener el servicion de localizacion
                stopLocationUpdates();
                return true;
            case R.id.example_seven_disconnect_action:
                // Servicion de localizacion
                desconectarServicioLocalizacion();
                return true;
            case R.id.example_eigth_connect_action:
                // Servicion de acelerómetro
                conectarServicioAcelerometro();
                return true;
            case R.id.example_eigth_disconnect_action:
                // Servicion de acelerómetro
                desconectarServicioAcelerometro();
                return true;
            case R.id.example_nine_init_action:
                servicioSOAP();
                return true;
            case R.id.example_ten_init_action:
                servicioREST();
                return true;
        }
        return false;
    }

    private void ejemploBarraProgreso() {
        // Mostramos un dialogo de barra de progreso
        mDialog = new ProgressDialog(this);
        mDialog.setMessage("Ejecutando tarea asincrónica...");
        mDialog.setTitle("Progreso");
        mDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mDialog.setCancelable(false);
        mDialog.setMax(100);
        mDialog.show();
        // Ejecutamos la tarea asincronica y actualizamos la barra de progreso
        new TaskBarraProgreso().execute();
    }

// Clase para la tarea asincronica que muestra la barra de progreso
// Params -> String -> doInBackground
// Progress -> Float -> onProgressUpdate
// Result -> Integer -> onPostExecute
    private class TaskBarraProgreso extends AsyncTask<String, Float, Integer> {
        @Override
        protected void onPreExecute() {
            // antes de ejecutar
            mDialog.setProgress(0);
        }

        @Override
        protected Integer doInBackground(String... params) {
            // Cuando se dispara la tarea simulamos el procesamiento en la tarea asincronica
            for (int i = 0; i < 100; i++) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {}
                // publicamos el progreso de la tarea
                publishProgress(i/100f);
            }
            return 100;
        }

        protected void onProgressUpdate (Float... values) {
            // actualizamos la barra de progreso y sus valores
            int p = Math.round(100 * values[0]);
            mDialog.setProgress(p);
        }

        protected void onPostExecute(Integer bytes) {
            // Cuando la tarea termina cerramos la barra de progreso
            mDialog.dismiss();
        }
    }

    private void ejemploVistaWeb() {
        // Solo visible el web view
        mWebView.setVisibility(View.VISIBLE);
        // configuro que el webview se abra dentro de la app
        mWebView.setWebViewClient(new WebViewClient());
        // opciones de configuracion del webview
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setLoadsImagesAutomatically(true);
        // Ejecutamos la tarea asincronica para mostrar el contenido de una pagina web  mediante un Runnable que
        // permite la ejecucion de tareas ejecutadas por un thread metodo HTTP como POST
        mWebView.post(new Runnable() {
            @Override
            public void run() {
                mWebView.loadUrl("http://www.google.com");
            }
        });
    }

    private void ejemploMostrarImagen() {
        // Solo visible la imagen
        mWebView.setVisibility(View.INVISIBLE);
        mImageView.setVisibility(View.VISIBLE);
        // Ejecutamos la tarea asincronica que baja y muestra la imagen con la ruta de descarga
        new TaskMostrarImagen().execute("https://www.ecci.ucr.ac.cr/sites/default/files/ecci-escuela-ciencias-computacion-informatica-horizontal_0.png");
    }

    // Clase para la tarea asincronica de Imagen
    private class TaskMostrarImagen extends AsyncTask<String, Void, Bitmap> {
        // La tarea se ejecuta en un thread tomando como parametro el enviado en AsyncTask.execute()
        @Override
        protected Bitmap doInBackground(String... urls) {
            // tomanos el parámetro del execute() y bajamos la imagen
            return loadImageFromNetwork(urls[0]);
        }

        // El resultado de la tarea tiene el archivo de imagen el cual mostramos
        protected void onPostExecute(Bitmap result) {
            mImageView.setImageBitmap(result);
        }

        // metodo para bajar la imagen
        private Bitmap loadImageFromNetwork(String url) {
            try {
                Bitmap bitmap = BitmapFactory.decodeStream((InputStream) new URL(url).getContent());
                return bitmap;
            } catch (Exception e) {
                Log.v(TAG_IMG, e.getMessage());
            }
            return null;
        }
    }

    // Servicio Http download
    private void startFragmentTask(String url) {
        // Solo visible el text view
        mDataText.setVisibility(View.VISIBLE);
        mWebView.setVisibility(View.INVISIBLE);
        mImageView.setVisibility(View.INVISIBLE);

        mDataText.setText(url);
        // Iniciamos el fragmento que ejecutamos en la tarea asincronica pasando el URL que buscamos su contenido
        mNetworkFragment = NetworkFragment.getInstance(getSupportFragmentManager(), url);
    }

    private void startDownload() {
        // si no estamos bajando contenido y el fragmento con la tarea fue inicializado
        if (!mDownloading && mNetworkFragment != null) {
            // Ejecutamos el download en la tarea async
            mNetworkFragment.startDownload();
            // Marcamos el download en proceso
            mDownloading = true;
        }
    }

    @Override
    public void finishDownloading() {
        // marcamos que finaliza el proceso de download
        mDownloading = false;
        // Cancelamos el fragmento con la tarea async
        if (mNetworkFragment != null) {
            mNetworkFragment.cancelDownload();
        }
        // Removemos el fragmento para poder iniciar luego una nueva petición distinta a la anterior
        NetworkFragment networkFragment = (NetworkFragment) getSupportFragmentManager().findFragmentByTag(NetworkFragment.TAG_FRAGMENT);
        if (networkFragment != null) {
            FragmentManager manager = getSupportFragmentManager();
            manager.beginTransaction().remove(networkFragment).commit();
            manager.popBackStack();
        }
    }

    // Actualizamos y mostramos los datos resultado del download
    @Override
    public void updateFromDownload(String result) {
        if (result != null) {
            mDataText.setText(result);
            Toast.makeText(getApplicationContext(), result, Toast.LENGTH_SHORT).show();
        } else {
            mDataText.setText(result);
            Toast.makeText(getApplicationContext(), "Error de Conexion",
                    Toast.LENGTH_SHORT).show();
        }
    }

    // Este metodo permite mostrar el estado de la tarea Async download
    @Override
    public void onProgressUpdate(int progressCode, int percentComplete) {
        switch (progressCode) {
            // You can add UI behavior for progress updates here.
            case Progress.ERROR:
                Toast.makeText(getApplicationContext(), "ERROR", Toast.LENGTH_SHORT).show();
                break;
            case Progress.CONNECT_SUCCESS:
                Toast.makeText(getApplicationContext(), "CONNECT_SUCCESS", Toast.LENGTH_SHORT).show();
                break;
            case Progress.GET_INPUT_STREAM_SUCCESS:
                Toast.makeText(getApplicationContext(), "GET_INPUT_STREAM_SUCCESS", Toast.LENGTH_SHORT).show();
                break;
            case Progress.PROCESS_INPUT_STREAM_IN_PROGRESS:
                Toast.makeText(getApplicationContext(), "PROCESS_INPUT_STREAM_IN_PROGRESS", Toast.LENGTH_SHORT).show();
                break;
            case Progress.PROCESS_INPUT_STREAM_SUCCESS:
                Toast.makeText(getApplicationContext(), "PROCESS_INPUT_STREAM_SUCCESS", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    // Informacion sobre el estado de la red
    @Override
    public NetworkInfo getActiveNetworkInfo() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo;
    }

    // Estado de la red, verificar el estado de la red
    public boolean isOnline() {
        ConnectivityManager connMgr = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    private void inicioServicio() {
        // Solo visible el text view
        mDataText.setVisibility(View.VISIBLE);
        mWebView.setVisibility(View.INVISIBLE);
        mImageView.setVisibility(View.INVISIBLE);
        mDataText.setText("");
        // Configuro para recibir mensajes desde el servicio
        LocalBroadcastManager.getInstance(this).registerReceiver((mReceiver), new IntentFilter(ServicioSimple.SERVICE_RESULT));
        // Inicio del servicio
        Intent serviceIntent = new Intent(this, ServicioSimple.class);
        serviceIntent.addCategory(ServicioSimple.SERVICE_TAG);
        startService(serviceIntent);
    }

    private void finServicio() {
        // Detener del servicio
        Intent serviceIntent = new Intent(this, ServicioSimple.class);
        serviceIntent.addCategory(ServicioSimple.SERVICE_TAG);
        stopService(serviceIntent);
        // Detengo recibir mensajes desde el servicio
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mReceiver);
    }

    // Creamos la instancia de GoogleAPIClient para los servicios de localizacion
    protected synchronized void buildGoogleApiClient() {
        Log.i(TAG, "Building GoogleApiClient");
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    // Creamos el request de localizaciones
    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        // Intervalo requerido
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        // La app no recibe notificaciones mas rapido que FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        // prioridad con la exactitud
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    // Chequeo si el dispositivo tiene la configuracion requerida
    protected void buildLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
    }

    private void conectarServicioLocalizacion() {
        if ((mGoogleApiClient != null) && !mGoogleApiClient.isConnected()) {
            mGoogleApiClient.connect();
            Toast.makeText(this, "mGoogleApiClient conectado", Toast.LENGTH_SHORT).show();
        }
    }

    private void desconectarServicioLocalizacion() {
        if ((mGoogleApiClient != null) && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
            Toast.makeText(this, "mGoogleApiClient desconectado", Toast.LENGTH_SHORT).show();
        }
    }

    // Servicios de localizacion
    @Override
    public void onLocationChanged(Location location) {
        mCurrentLocation = location;
        mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
        // Actualizar la interfaz de usuario
        updateUI();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    // Servicios de localizacion
    protected void startLocationUpdates() {
        mDataText.setText("Inicio GPS:\n");

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        if (ActivityCompat.checkSelfPermission(
                this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
            setLocationPermissions();
            return;
        }


        Task<Location> task = fusedLocationClient.getLastLocation();
        task.addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if(location!=null) {
                    mCurrentLocation = location;
                    mLastUpdateTime = DateFormat.getDateTimeInstance().format(new Date());
                    updateUI();
                }
            }
        });
        fusedLocationClient.requestLocationUpdates(mLocationRequest, locationCallback, null);
    }

    // Servicios de localizacion
    private void updateUI() {
        if (mCurrentLocation != null) {
            // contatenamos los datos de localizacion recibidos
            String txt = mDataText.getText().toString();
            txt += " /*/Servicio de Localización/*/ \n" +
                    "Lat: " + String.valueOf(mCurrentLocation.getLatitude()) + "\n" +
                    "Lon: " + String.valueOf(mCurrentLocation.getLongitude()) + "\n" +
                    "Time: " + mLastUpdateTime + "\n";

            mDataText.setText(txt);
        }
    }

// Servicios de localizacion
// si la actividad pausa debe detener el servicio de localizacion
    @Override
    protected void onPause() {
        super.onPause();
//        stopLocationUpdates();
    }

    // Servicios de localizacion
    @Override
    protected void onStop() {
        super.onStop();
        stopLocationUpdates();
    }

    // Servicios de localizacion
    public void iniciarServicioLocalizacion() {
        // Solo visible el text view
        mDataText.setVisibility(View.VISIBLE);
        mWebView.setVisibility(View.INVISIBLE);
        mImageView.setVisibility(View.INVISIBLE);
        // Iniciamos el servicio de localizacion
        if (!mRequestingLocationUpdates) {
            mRequestingLocationUpdates = true;
            startLocationUpdates();
            Toast.makeText(this, "Inicio de servicios de localización", Toast.LENGTH_SHORT).show();
        }
    }

    private void setLocationPermissions() {
        ActivityCompat.requestPermissions(this, new String[] {
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
        },PERMISSION_REQUEST_LOCATION);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults){
        switch (requestCode){
            case PERMISSION_REQUEST_LOCATION:{
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startLocationUpdates();
                }
            }
        }
    }

    // Servicios de localizacion
    protected void stopLocationUpdates () {
        fusedLocationClient.removeLocationUpdates(locationCallback);
        mRequestingLocationUpdates = false;
        Toast.makeText(this, "Fin de servicios de localizacion", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.i(TAG, "Connection establish");
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "Connection suspended");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.i(TAG, "Connection failed: " + connectionResult.getErrorCode());
    }

    // Servicio acelerómetro
    private void conectarServicioAcelerometro() {
        // No mostramos los demás componentes
        mDataText.setVisibility(View.VISIBLE);
        mWebView.setVisibility(View.INVISIBLE);
        mImageView.setVisibility(View.INVISIBLE);
        // Administrador de sensores
        SensorManager sm = (SensorManager) getSystemService(SENSOR_SERVICE);
        // Obtenemos el sensor TYPE_ACCELEROMETER
        List<Sensor> sensors = sm.getSensorList(Sensor.TYPE_ACCELEROMETER);
        // A partir del sensor solicitamos escuchar sus valores
        if (sensors.size() > 0) {
            // escuchamos los valores del sensor con una velocidad SENSOR_DELAY_NORMAL
            sm.registerListener(this, sensors.get(0), SensorManager.SENSOR_DELAY_UI);
        }
    }

    private void desconectarServicioAcelerometro() {
        // Detener el listener
        SensorManager sm = (SensorManager) getSystemService(SENSOR_SERVICE);
        sm.unregisterListener(this);
    }

    // llamada cuando existe un nuevo evento del sensor
    @Override
    public void onSensorChanged(SensorEvent event) {
        // Sincronizado para evitar problemas de concurrencia entre los threads
        synchronized (this) {
            // momento del evento
            long mTimeStamp = event.timestamp;
            // valores actuales
            actualX = event.values[0];
            actualY = event.values[1];
            actualZ = event.values[2];
            // compara con valores previos
            if (previoX == 0 && previoY == 0 && previoZ == 0) {
                ultimaActualizacion = mTimeStamp;
                previoX = actualX;
                previoY = actualY;
                previoZ = actualZ;
            }
            // reporta valores cada N tiempo
            long time_difference = mTimeStamp - ultimaActualizacion;
            if (time_difference > 1000000000L) {
                float movementX = Math.abs(actualX - previoX);
                float movementY = Math.abs(actualY - previoY);
                float movementZ = Math.abs(actualZ - previoX);
                float min_movement = 1.5f;
                // reporta si hay movimiento
                if (movementX > min_movement) {
                    // reportamos el movimiento
                    String txt = mDataText.getText().toString();
                    txt += "\n /*/Hay Movimiento X/*/ \n\n";
                    mDataText.setText(txt);
                }
                // reporta si hay movimiento
                if (movementY > min_movement) {
                    // reportamos el movimiento
                    String txt = mDataText.getText().toString();
                    txt += "\n /*/Hay Movimiento Y/*/ \n\n";
                    mDataText.setText(txt);
                }
                // reporta si hay movimiento
                if (movementZ > min_movement * 3) {
                    // reportamos el movimiento
                    String txt = mDataText.getText().toString();
                    txt += "\n /*/Hay Movimiento Z/*/ \n\n";
                    mDataText.setText(txt);
                }
                previoX = actualX;
                previoY = actualY;
                previoZ = actualZ;
                ultimaActualizacion = mTimeStamp;
                // reportamos los valores
                String txt = mDataText.getText().toString();
                txt += " /*/Servicio de Acelerómetro/*/ \n" +
                        "X: " + actualX +
                        "\n" + "Y: " + actualY +
                        "\n" + "Z: " + actualZ +
                        "\n";
                mDataText.setText(txt); // mDataText.append("\n" + txt);
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) { }

    // *********************************************************************************************
    private void servicioSOAP() {
        // No mostramos los demás componentes
        mDataText.setVisibility(View.VISIBLE);
        mWebView.setVisibility(View.INVISIBLE);
        mImageView.setVisibility(View.INVISIBLE);

        mDataText.setText("/*/ Llamada a web service SOAP /*/ \n");
        // llamada a servicio en un background thread
        Thread mJob = new Thread() {
            @Override public void run() {
                String resultValue = "\n GetGeoIP: " + "74.125.91.106" + "\n";
                // Servicio GetGeoIP
                try {
                    ResultadoGeoIPService mGeoIPService = GetGeoIP("74.125.91.106", NAMESPACE, METHOD_NAME, SOAP_ACTION, URL);
                    resultValue = resultValue + "\n \n" + mGeoIPService.toString();
                } catch (Exception e) {
                    resultValue = resultValue + "\n \n" + "ERROR: " + e.getMessage();
                }
                // enviamos el mensaje con handler a la UI
                Message msg = handler.obtainMessage();
                msg.obj = resultValue;
                handler.sendMessage(msg);
            }
        };
        // inicio el trabajo para realizar la peticion
        mJob.start();
    }

    public ResultadoGeoIPService GetGeoIP(String mIP, String mNamespace, String mMetodo, String mAccion, String mUrl) throws Exception{
        try {
            // preparar el SOAP REQUEST (namespace, method, arguments)
            SoapObject mRequest = new SoapObject(mNamespace, mMetodo);
            // pasamos el parametro de entrada IPAddress 74.125.91.106
            PropertyInfo p = new PropertyInfo();
            p.setName("IPAddress"); p.setValue(mIP);
            p.setType(String.class); mRequest.addProperty(p);
            // preparamos el ENVELOPE request
            SoapSerializationEnvelope mEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            mEnvelope.setOutputSoapObject(mRequest);
            mEnvelope.dotNet = true;
            // Llamamos al servicio
            HttpTransportSE mTransporte = new HttpTransportSE(mUrl);
            mTransporte.debug = true;
            mTransporte.call(mAccion, mEnvelope);
            //*// //*String mResponse = mEnvelope.getResponse().toString();
            // Recibimos el objeto con la respuesta
            SoapObject resultado = (SoapObject) mEnvelope.getResponse();
            ResultadoGeoIPService mResultadoGeoIPService = new ResultadoGeoIPService();
            mResultadoGeoIPService.setNombrePais(resultado.getProperty("CountryName").toString());
            mResultadoGeoIPService.setCodigoPais(resultado.getProperty("CountryCode").toString());
            return mResultadoGeoIPService;
        }catch (Exception e){
            throw(e);
        }
    }

    // Usamos un handler to keep GUI update on behalf of background tasks
    Handler handler = new Handler() {
        @Override public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String txt = (String) msg.obj;
            mDataText.append("\n" + txt);
        }
    };

    private void servicioREST() {
        // No mostramos los demás componentes
        mDataText.setVisibility(View.VISIBLE);
        mWebView.setVisibility(View.INVISIBLE);
        mImageView.setVisibility(View.INVISIBLE);

        mDataText.setText("/*/ Llamada a web service REST /*/ \n");

        mDataText.setText(URL_REST);
        // Tarea AsyncTask para ejecutar la solicitud
        new TaskServicioREST().execute(URL_REST);
    }

    // Clase para la tarea asincronica de Gson en Servicio REST
    private class TaskServicioREST extends AsyncTask<String, Void, String> {
        // La tarea se ejecuta en un thread tomando como parametro el eviado en
        // AsyncTask.execute()
        @Override
        protected String doInBackground(String... urls) {
            // tomanos el parámetro del execute() y bajamos el contenido
            return loadContentFromNetwork(urls[0]);
        }

        // El resultado de la tarea tiene el archivo gson el cual mostramos
        protected void onPostExecute(String result) {
            mDataText.append("\n \n" + result);
        }

        // metodo para bajar el contenido
        private String loadContentFromNetwork(String url) {
            try {
                InputStream mInputStream = (InputStream) new URL(url).getContent();
                InputStreamReader mInputStreamReader = new InputStreamReader(mInputStream);
                BufferedReader responseBuffer = new BufferedReader(mInputStreamReader);
                StringBuilder strBuilder = new StringBuilder(); String line = null;
                while ((line = responseBuffer.readLine()) != null) {
                    strBuilder.append(line);
                }
                //
                // *Gson mJson = new Gson();
                // *mJson.fromJson(strBuilder.toString(),);//,YourClass.class);
                // //*return mJson.toString(); //strBuilder.toString();
                return strBuilder.toString();
            } catch (Exception e) {
                Log.v(TAG_IMG, e.getMessage());
            }
            return null;
        }
    }
}


